OTT=ott
OTT_OPTIONS=-merge true -coq_expand_list_types false

OTT_FILES = \
  header.ott \
  base.ott \
  arith.ott \
  string.ott \
  bytes.ott \
  lambda.ott \
  crypto.ott \
  operation.ott \
  list.ott \
  map.ott \
  record.ott \
  variant.ott \
  macros.ott

all: albert.vo

albert.v : $(OTT_FILES)
	$(OTT) $(OTT_OPTIONS) -o $@ $^

%.vo: %.v
	coqc $<

clean:
	rm -f albert.*
